package day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import Invoice.bean.Customer;

public class InvoiceProject {

	public static void main(String[] args) {
		
		List<Customer> customers = new ArrayList<Customer>();
		customers.add(new Customer("deepak", "Chennai", "deepak@dummy.com", "883837372"));
		customers.add(new Customer("prem", "Delhi", "prem@test.com", "883837372"));
		customers.add(new Customer("db", "Mumbai", "db@gmail.com", "377990372"));
		customers.add(new Customer("arun", "fMumbai", "arun@gmail.com", "7789767372"));

		//Collections.sort(customers,new CustomerComparator());
		
		for(Customer customer : customers)
			System.out.println(customer);
		
		System.out.println();
		
		List<Employee> employees = new ArrayList<Employee>();
		employees.add(new Employee(1, "Shalini", "Mumbai"));
		employees.add(new Employee(5, "Alok", "Agra"));
		employees.add(new Employee(3, "Ambati", "Delhi"));
		employees.add(new Employee(2, "Deepak", "Pune"));
		employees.add(new Employee(4, "Nisha", "Chennai"));
		
		for(Employee employee : employees)
			System.out.println(employee);
		
		Collections.sort(employees);
		System.out.println();
		for(Employee employee : employees)
			System.out.println(employee);
	}

}
