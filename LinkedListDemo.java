package day1;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class LinkedListDemo {

	public static void main(String[] args) {
		List<Integer> ls = new LinkedList<Integer>();
		Scanner ob = new Scanner(System.in);
		int i = 0;
		System.out.println("Enter no of elements : ");
		int num = ob.nextInt();
		System.out.println("Enter elements into array : ");
		while(i < num && ob.hasNextInt()) {
			ls.add(ob.nextInt());
			i++;
		}
		Collections.sort(ls);
		System.out.println(ls);
		Collections.reverse(ls);
		System.out.println(ls);
		

	}

}
