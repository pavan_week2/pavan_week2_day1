package day1;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
public class ListDemo {

	public static void main(String[] args) {
		
		List<Integer> ls = new ArrayList<Integer>();
		ls.add(2);
		ls.add(12);
		ls.add(24);
		ls.add(3);
		ls.add(15);
		System.out.println("Initial Size : " + ls.size());
		System.out.println("Removed element by index : " + ls.remove(2));
		System.out.println("Removing element by passing value : " + ls.remove(Integer.valueOf(15)));
		System.out.println("After removing Size : " + ls.size());
		System.out.println("Min Element : " + Collections.min(ls));
		System.out.println("Max Element : " + Collections.max(ls));
		Collections.sort(ls);
		for(Integer n : ls) {
			System.out.println(n);
		}
		
		
		
		

	}

}
