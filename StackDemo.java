package day1;

import java.util.Scanner;
import java.util.Stack;

public class StackDemo {

	public static void main(String[] args) {
		Stack<String> stack = new Stack<>();
		Scanner ob = new Scanner(System.in);
		System.out.println("Enter no of names to pushed : ");
		int num = ob.nextInt();
		System.out.println("Enter names : ");
		for(int i = 0 ; i < num ; i++) {
			stack.push(ob.next());
		}
		System.out.println("Stack : "+stack);
		System.out.println("Names over iterator : ");
		for(String s : stack) {
			System.out.println(s);
		}
		

	}

}
